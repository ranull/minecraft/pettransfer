package com.ranull.pettransfer.pet;

import com.ranull.pettransfer.PetTransfer;
import org.apache.commons.lang.WordUtils;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;

public class PetManager {
	private PetTransfer plugin;

	public PetManager(PetTransfer plugin) {
		this.plugin = plugin;
	}
	
	public String getName(Tameable tameable) {
		String type = WordUtils.capitalizeFully(tameable.getType().toString()).replace("_", " ");
		return type;
	}

	public void transfer(Player oldOwner, Player newOwner, Tameable pet) {
		pet.setOwner(newOwner);

		String transferOldOwner = plugin.getConfig().getString("settings.transferOldOwner").replace("$type", getName(pet)).replace("$newOwner", newOwner.getName()).replace("&", "§");
		String transferNewOwner = plugin.getConfig().getString("settings.transferNewOwner").replace("$type", getName(pet)).replace("$oldOwner", oldOwner.getName()).replace("&", "§");

		oldOwner.sendMessage(transferOldOwner);
		newOwner.sendMessage(transferNewOwner);
	}
}
