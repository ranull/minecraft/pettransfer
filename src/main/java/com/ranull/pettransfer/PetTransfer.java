package com.ranull.pettransfer;

import org.bukkit.plugin.java.JavaPlugin;

import com.ranull.pettransfer.commands.PetTransferCommand;
import com.ranull.pettransfer.events.Events;
import com.ranull.pettransfer.pet.PetManager;

public class PetTransfer extends JavaPlugin {
	@Override
	public void onEnable() {
		saveDefaultConfig();

		PetManager petManager = new PetManager(this);

		getCommand("pettransfer").setExecutor(new PetTransferCommand(this));

		getServer().getPluginManager().registerEvents(new Events(this, petManager), this);
	}
}