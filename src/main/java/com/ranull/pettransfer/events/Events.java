package com.ranull.pettransfer.events;

import com.ranull.pettransfer.PetTransfer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.ranull.pettransfer.pet.PetManager;

public class Events implements Listener {
	private PetTransfer plugin;
	private PetManager petManager;

	public Events(PetTransfer plugin, PetManager petManager) {
		this.plugin = plugin;
		this.petManager = petManager;
	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (event.getRightClicked() instanceof Tameable) {
			Tameable tameable = (Tameable) event.getRightClicked();

			Player oldOwner = event.getPlayer();

			if (!oldOwner.hasPermission("pettransfer.transfer")) {
				return;
			}

			String transferMaterial = plugin.getConfig().getString("settings.transferMaterial");
			Material material = Material.getMaterial(transferMaterial);

			if (material == null) {
				event.getPlayer().sendMessage("Material ERROR: " + transferMaterial);
				return;
			}

			ItemStack hand = oldOwner.getInventory().getItemInMainHand();

			if (!hand.getType().equals(material)) {
				return;
			}

			String handName = hand.getItemMeta().getDisplayName();

			Player newOwner = Bukkit.getServer().getPlayer(handName);

			if (tameable.getOwner() == null) {
				return;
			}

			if (!tameable.getOwner().equals(oldOwner)) {
				return;
			}

			if (event.getHand() != EquipmentSlot.HAND) {
				return;
			}

			if (!hand.getItemMeta().hasDisplayName()) {
				return;
			}

			if (newOwner == null) {
				return;
			}

			petManager.transfer(oldOwner, newOwner, tameable);

			hand.setAmount(hand.getAmount() - 1);

			event.setCancelled(true);
		}
	}
}
