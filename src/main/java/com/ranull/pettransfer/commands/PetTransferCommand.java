package com.ranull.pettransfer.commands;

import com.ranull.pettransfer.PetTransfer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class PetTransferCommand implements CommandExecutor {
	private PetTransfer plugin;

	public PetTransferCommand(PetTransfer plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String version = "1.3";
		String author = "Ranull";

		if (args.length < 1) {
			sender.sendMessage(
					ChatColor.DARK_GRAY + "» " + ChatColor.GOLD + "PetTransfer " + ChatColor.GRAY + "v" + version);
			sender.sendMessage(
					ChatColor.GRAY + "/pettransfer " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");
			if (sender.hasPermission("pettransfer.reload")) {
				sender.sendMessage(ChatColor.GRAY + "/pettransfer reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
						+ " Reload plugin");
			}
			sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
			return true;
		}
		if (args.length == 1 && args[0].equals("reload")) {
			if (!sender.hasPermission("pettransfer.reload")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "PetTransfer" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " No Permission!");
				return true;
			}
			plugin.reloadConfig();
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "PetTransfer" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " Reloaded config file!");
		}
		return true;
	}
}
